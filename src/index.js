import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; // Importa BrowserRouter

ReactDOM.render(
	<Router>
		{' '}
		{/* Envuelve tu App en un Router */}
		<Routes>
			<Route path='/' element={<App />} />
			{/* <Route path='/animalesSinCors' element={<ASC />} />
			<Route path='/animalesConCors' element={<ACC />} />
			<Route path='/apiSinCors' element={<APIN />} /> */}
		</Routes>
	</Router>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
