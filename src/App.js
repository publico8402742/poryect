import React from 'react';
import './App.css';
import { useNavigate } from 'react-router-dom';

const App = () => {
	const navigate = useNavigate();

	// const handleASinClick = () => {
	// 	navigate('/apiSinCors');
	// };

	return (
		<div className='App'>
			<header className='App-header'>
				<div style={{ gap: 6, display: 'flex' }}></div>
				<div>ola</div>
			</header>
		</div>
	);
};

export default App;
